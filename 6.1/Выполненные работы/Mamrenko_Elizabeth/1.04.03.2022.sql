drop table if exists equipment cascade;
drop table if exists holder cascade;

create table equipment
    (
        title text,
        count int,
        "holderPhone" text
    );

create table holder
    (
        name text,
        phone text
    );


insert into holder (name, phone)
values ('Альберт Андреевич', '0001'),
       ('Иван Вячеславович', '0002'),
       ('Вячеслав Александрович', '0003');


insert into equipment (title, count, "holderPhone")
values ('Ракетка', 2, '0003'),
       ('Мяч', 3, '0001'),
       ('Палатка', 6, '0003'),
       ('Удочка', 2, '0001'),
       ('Мангал', 1, '0002'),
       ('Спальник', 5, '0001'),
       ('Рюкзак', 10, '0002'),
       ('Дождевик', 10, '0002'),
       ('Компас', 1, '0003'),
       ('Термос', 4, '0001');
