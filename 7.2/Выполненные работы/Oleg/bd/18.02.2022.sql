drop table if exists Product cascade;
drop table if exists PC;
drop table if exists Laptop;
drop table if exists Printer;

create table Product
(
   maker varchar(10),
   model varchar(50) primary key,
   type varchar(50)
);
create table PC
(
   code int primary key,
   model varchar(50) references product (model),
   speed int,
   ram int,
   hd real,
   cd varchar(10),
   price money
);
create table Laptop
(
   code int primary key,
   model varchar(50) references product (model),
   speed int,
   ram int,
   hd real,
   price money,
   screen int
);
create table Printer
(
   code int primary key,
   model varchar(50) references product (model),
   color char(1),
   type varchar(10),
   price money
);
insert into Product values ('B', '1121', 'PC'),
                           ('A', '1232', 'PC'),
                           ('A', '1233', 'PC'),
                           ('E', '1260', 'PC'),
                           ('A', '1276', 'Printer'),
                           ('D', '1288', 'Printer'),
                           ('A', '1298', 'Laptop'),
                           ('C', '1321', 'Laptop'),
                           ('A', '1401', 'Printer'),
                           ('A', '1408', 'Printer'),
                           ('D', '1433', 'Printer'),
                           ('E', '1434', 'Printer'),
                           ('B', '1750', 'Laptop'),
                           ('A', '1752', 'Laptop'),
                           ('E', '2113', 'PC'),
                           ('E', '2112', 'PC');

insert into PC values (1, '1232', 500, 64, 5.0, '12x', 600.00),
                      (2, '1121', 750, 128, 14.0, '40x', 850.00),
                      (3, '1233', 500, 64, 5.0, '12x', 600.00),
                      (4, '1121', 600, 128, 14.0, '40x', 850.00),
                      (5, '1121', 600, 128, 8.0, '40x', 850.00),
                      (6, '1233', 750, 128, 20.0, '50x', 950.00),
                      (7, '1232', 500, 32, 10.0, '12x', 400.00),
                      (8, '1232', 450, 64, 8.0, '24x', 350.00),
                      (9, '1232', 450, 32, 10.0, '24x', 350.00),
                      (10,'1260', 500, 32, 10.0, '12x', 350.00),
                      (11, '1233', 900, 128, 40.0, '40x', 980.00),
                      (12, '1233', 800, 128, 20.0, '50x', 970.00);

insert into Laptop values (1, '1298', 350, 32, 4.0, 700.00, 11),
                          (2, '1321', 500, 64, 8.0, 970.00, 12),
                          (3, '1750', 750, 128, 12.0, 1200.00, 14),
                          (4, '1298', 600, 64, 10.0, 1050.00, 15),
                          (5, '1752', 750, 128, 10.0, 1150.00, 14),
                          (6, '1298', 450, 64, 10.0, 950.00, 12);

insert into Printer values (1, '1276', 'n', 'Laser', 400.00),
                           (2, '1433', 'y', 'Jet', 270.00),
                           (3, '1434', 'y', 'Jet', 290.00),
                           (4, '1401', 'n', 'Matrix', 150.00),
                           (5, '1408', 'n', 'Matrix', 270.00),
                           (6, '1288', 'n', 'Laser', 400.00);

SELECT model, ram, screen FROM Laptop WHERE price > CAST('1000' AS MONEY);
SELECT model, speed, hd FROM PC WHERE price < CAST('500' AS MONEY);
SELECT model, speed, hd FROM PC WHERE price < CAST('600' AS MONEY) and  ( cd = '12x' OR cd = '24x' ) ;
SELECT * FROM Printer WHERE color = 'y';
