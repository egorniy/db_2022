drop table if exists tour cascade;
drop table if exists city cascade;
drop table if exists review cascade;
drop table if exists "CityToTour" cascade;

create table tour
(
	name varchar(128)
		constraint "PK_name_tour" primary key
);

create table city
(
	name varchar(128)
		constraint "PK_name_city" primary key
);

create table review
(
	comment varchar(128),
	rating integer not null,
	tour varchar(128)
		constraint "FK_tour_review" references tour not null
);

create table "CityToTour"
(
	"NameTour" varchar(128) not null,
	"NameCity" varchar(128) not null,
		constraint "FK_nameCity_toCity" foreign key("NameCity") references city,
		constraint "FK_nameTour_toTour" foreign key("NameTour") references tour
);

alter table "CityToTour"
	add constraint "UQ_CityToTour" unique("NameTour", "NameCity");

insert into tour
values  ('В гостях у Хаски'),
        ('Хиты карелии');

insert into city
values  ('Приозерск'),
        ('Сартовала'),
        ('Валаам');

insert into "CityToTour"
values  ('В гостях у Хаски', 'Приозерск'),
        ('В гостях у Хаски', 'Сартовала'),
        ('Хиты карелии', 'Сартовала'),
        ('Хиты карелии', 'Валаам');

insert into review
values  ('Отличный тур', 5, 'Хиты карелии'),
        ('Не понравилось', 2, 'Хиты карелии');

select "NameTour", "NameCity", comment, rating
from tour
		join "CityToTour" on tour.name = "CityToTour"."NameTour"
		join city on city.name = "CityToTour"."NameCity"
		join review on tour.name = review.tour;

alter table tour add column price numeric(15, 6);
alter table city add column founded text;
alter table review add column id int;

update tour
set price = 15650
where name = 'Хиты карелии';

update tour
set price = 9870
where name = 'В гостях у Хаски';

update city
set founded = 1295
where name = 'Приозерск';

update city
set founded = 1407
where name = 'Валаам';

update city
set founded = 1468
where name = 'Сартовала';

update review
set id = 1
where comment = 'Отличный тур' and rating = 5 and tour = 'Хиты карелии';

update review
set id = 2
where comment = 'Не понравилось' and rating = 2 and tour = 'Хиты карелии';
