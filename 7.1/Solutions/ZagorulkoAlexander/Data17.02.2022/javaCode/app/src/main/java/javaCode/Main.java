package javaCode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;
import java.util.HashMap;

public class Main{
    public static void main(String[] args) throws SQLException{
        
        //connection
        Properties connectionProps = new Properties();
        connectionProps.put("user", "postgres");
        connectionProps.put("password", "123");
        String url = "jdbc:postgresql://127.0.0.1:5432/postgres";
        Connection conn = DriverManager.getConnection(url, connectionProps);
        System.out.println("Connected to database");

        Statement stmt = null;
        String selectTableSQL = "SELECT name as \"holderName\", phone as \"holderPhone\", title as \"equipmentTitle\", count as \"equipmentCount\" from holder join equipment on phone = \"holderPhone\"";

        ArrayList<Equipment> equipments = new ArrayList<>();
        HashMap<String, Holder> map = new HashMap();

        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(selectTableSQL);
            
            //creating class instances
            while (rs.next()) {
                String holderName = rs.getString("holderName");
                String holderPhone = rs.getString("holderPhone");
                String equipmentTitle = rs.getString("equipmentTitle");
                int equipmentCount = rs.getInt("equipmentCount");
                
                if (map.containsKey(holderName)){
                    Holder holder = map.get(holderName);
                    Equipment equipment = new Equipment(equipmentTitle, equipmentCount, holder);
                    equipments.add(equipment);
                    holder.addEquipment(equipment);
                } else {
                    Holder holderNew = new Holder(holderName, holderPhone);
                    Equipment equipment = new Equipment(equipmentTitle, equipmentCount, holderNew);
                    equipments.add(equipment);
                    holderNew.addEquipment(equipment);
                    map.put(holderName, holderNew);
                }
            }

        } catch (SQLException e ) {
            System.out.println(e.getMessage());
        } finally {
            if (stmt != null) { stmt.close(); }
        }
    }
}