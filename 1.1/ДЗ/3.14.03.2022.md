﻿## 1. Создать таблицы и связи между ними, соответствующие классам ([связь many-to-many](https://gitlab.com/golodnyuk.iv/db_2022/-/blob/main/%D0%9C%D0%B0%D1%82%D0%B5%D1%80%D0%B8%D0%B0%D0%BB%D1%8B%20%D0%BF%D0%BE%20%D0%BA%D1%83%D1%80%D1%81%D1%83/3.%20%D0%A1%D0%B2%D1%8F%D0%B7%D1%8C%20many%20to%20many.md)):

### тур может проходить через несколько городов
### у тура может быть несколько отзывов
```python
class Tour:
    name: str
    price: decimal
    cities: List[City]
    reviews: List[Review]
```

### через город может проходить несколько туров
```python
class City:
    name: str
    founded: str
    tours: List[Tour]
```

### отзыв может относится только к одному туру
```python
class Review:
    id: int
    comment: str
    ratio: int
    tour: Tour
```

### связи между таблицами:
`Tour <-> City: many to many`\
`Tour <- Review: one to many, Review зависит от Tour`


## 2. Заполнить таблицы данными:
тур "В гостях у хаски", города: [Приозерск(1295), Сортавала(1468)];\
тур "Хиты карелии", города: [Сортавала, Валаам(1407)], отзывы: [("Отличный тур!", 5), ("Не понравилось.", 2)]

## 3. Вывести все туры с их городами и отзывами.
*Примечание:* т.к. названия столбцов у таблиц повторяются, в селекте нужно явно указывать к какой таблице они относятся. Например:
```sql
select *
from tour
         join "cityToTour" on tour.name = "cityToTour"."tourName"
         join city on city.name = "cityToTour"."cityName";
```
*Подсказка:* нужно использовать `left join`

## 4. На языке Python через коннектор выполнить запрос на вывод всех туров с их городами и отзывами и результат десериализовать в экземпляры классов.
```python
class Tour:
    name: str
    price: decimal
    cities: List[City]
    reviews: List[Review]

class City:
    name: str
    founded: str
    tours: List[Tour]

class Review:
    id: int
    comment: str
    ratio: int
    tour: Tour
```